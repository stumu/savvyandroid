package net.theroyalwe.savvy;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.SupportMapFragment;


public class SavvyMapFragment extends SupportMapFragment{
	
	private LocationManager locationManager;
	private LocationListener locationListener;
	private String bestProvider;
	private LatLng userLatLng;
	private GoogleMap map = null;
	//private MyLocationOverlay me = null;
	
	//private MapController mMapController;
	
	/**
	 * Create a new instance of MapFragment, initialized to
	 * show the location Entry at 'index'.
	 */
	public static SavvyMapFragment newInstance(int index) {
		SavvyMapFragment f = new SavvyMapFragment();

		// Supply index input as an argument.
		Bundle args = new Bundle();
		args.putInt("index", index);
		f.setArguments(args);

		return f;
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		
		map = this.getMap();
		Log.d("MAP", "Map created");
		// Acquire a reference to the system Location Manager
		LocationManager locationManager = (LocationManager) getActivity()
				.getApplication().getSystemService(Context.LOCATION_SERVICE);
		// Define a listener that responds to location updates
		locationListener = new LocationListener() {

			public void onLocationChanged(Location location) {
				// Called when a new location is found by the location provider.
				double lat = location.getLatitude();
				double lon = location.getLongitude();
				userLatLng = new LatLng(lat, lon);
				android.util.Log.i("MAP", "Location changed");
			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}
		};
		// Register the listener with the Location Manager to receive location
		// updates as frequently as possible
				if (locationManager.getAllProviders().contains(
						LocationManager.NETWORK_PROVIDER)) {
					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER, 0, 3, locationListener);
				}
				if (locationManager.getAllProviders().contains(
						LocationManager.GPS_PROVIDER)) {
					locationManager.requestLocationUpdates(
							LocationManager.GPS_PROVIDER, 0, 3, locationListener);
				}
		
		/*map.getController().setCenter(getPoint(40.76793169992044,
                                            -73.98180484771729));
    map.getController().setZoom(17);
    map.setBuiltInZoomControls(true);

    Drawable marker=getResources().getDrawable(R.drawable.marker);

    marker.setBounds(0, 0, marker.getIntrinsicWidth(),
                            marker.getIntrinsicHeight());

    map.getOverlays().add(new SitesOverlay(marker));

    me=new MyLocationOverlay(getActivity(), map);
    map.getOverlays().add(me);*/
		
		
	}
	
	
}


