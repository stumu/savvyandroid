package net.theroyalwe.savvy;

import com.actionbarsherlock.app.SherlockFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CategoryInfoFragment extends SherlockFragment{
	
	SavvyCategory mCategory;
	
	@Override
	public void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		mCategory = (SavvyCategory) getArguments().get(getString(R.string.CATEGORY_KEY));
	}
	
	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		return inflater.inflate(R.layout.category_view, container,false);
	}
	
	@Override
	public void onActivityCreated (Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		TextView title = (TextView) getView().findViewById(R.id.categoryInfoTitle);
		title.setText(mCategory.getName());
		
		ImageView image = (ImageView) getView().findViewById(R.id.categoryInfoImage);
		image.setImageBitmap(mCategory.getImage());
		
	}
}
