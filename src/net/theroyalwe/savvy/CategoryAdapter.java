package net.theroyalwe.savvy;

import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CategoryAdapter extends ArrayAdapter<SavvyCategory> {

	Context context;
	int layoutResourceId;
	SavvyCategory data[] = null;

	public CategoryAdapter(Context context, int layoutResourceId,
			SavvyCategory[] data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		CategoryHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new CategoryHolder();
			holder.imgIcon = (ImageView) row.findViewById(R.id.imgIcon);
			holder.txtTitle = (TextView) row.findViewById(R.id.txtTitle);

			row.setTag(holder);
		} else {
			holder = (CategoryHolder) row.getTag();
		}

		SavvyCategory category = data[position];
		holder.txtTitle.setText(category.getName());
		//Asynchronously load image from the url
		if(category.getImage() == null){
			new DownloadImageTask(holder.imgIcon, category).execute(category.getPicUrl());
		} else {
			holder.imgIcon.setImageBitmap(category.getImage());
		}

		return row;
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;
		SavvyCategory category;

		public DownloadImageTask(ImageView bmImage, SavvyCategory categoryToUpdate) {
			this.bmImage = bmImage;
			this.category = categoryToUpdate;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
			category.image = result;
		}
	}

	static class CategoryHolder {
		ImageView imgIcon;
		TextView txtTitle;
	}
}
