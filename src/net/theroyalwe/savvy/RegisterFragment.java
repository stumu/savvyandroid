package net.theroyalwe.savvy;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.app.SherlockFragment;

public class RegisterFragment extends SherlockFragment {
	private static String TAG = "Register";

	EditText textName;
	EditText textEmail;
	String name;
	String email;
	String regId;

	Button buttonRegister;
	SharedPreferences prefs;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		prefs = getActivity().getSharedPreferences("pref", 0);

		View myFragmentView = inflater.inflate(R.layout.register_user,
				container, false);

		textName = (EditText) myFragmentView.findViewById(R.id.txtName);
		textEmail = (EditText) myFragmentView.findViewById(R.id.txtEmail);
		buttonRegister = (Button) myFragmentView.findViewById(R.id.btnRegister);

		if (prefs.getString("name", "") != null) {

			textName.setText(prefs.getString("name", ""));
		}
		if (prefs.getString("email", "") != null) {
			textEmail.setText(prefs.getString("email", ""));
		}

		buttonRegister.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				name = textName.getText().toString();
				email = textEmail.getText().toString();

				if (name.trim().length() > 0 && email.trim().length() > 0) {
					// launch/load the new post intent

					// send register parameters to backend

					// execute background task...

					new RegisterUserTask().execute();

				} else {
					// alert.showAlertDialog(RegisterFragment.this,"Registration Error! Try again");
					Log.v(TAG, "error registering, make this a real alert!");

				}
			}
		});
		// {

		// }

		return myFragmentView;
	}

	private class RegisterUserTask extends AsyncTask<String, Integer, Boolean> {

		@Override
		protected void onPreExecute() {
			// show a loader

		}

		@Override
		protected void onPostExecute(Boolean result) {
			// dissmiss this

			// view as modal or load the new post view or intent or whatever...
			// save the settings

			prefs.edit().putString("name", name);
			prefs.edit().putString("email", email);

		}

		@Override
		protected Boolean doInBackground(String... arg0) {
			// TODO Auto-generated method stub

			try {

				Log.v(TAG, "Registering user");

				new SavvyAPI().registerUser(name, email, regId);
				
			} catch (ClientProtocolException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}

			return null;
		}
	}

}
