package net.theroyalwe.savvy;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SavvyAPI {

	private static String BASE_URL = "http://savvy.theroyalwe.net";
	private static String SECURE_BASE_URL = "https://www.theroyalwe.net/~savvy/";

	HttpClient client = new DefaultHttpClient();

	// HttpResponse response;

	private String processResponse(HttpResponse response) {

		StringBuilder stringBuilder = new StringBuilder();
		try {

			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return stringBuilder.toString();

	}

	private String makeGetRequest(String url) throws ClientProtocolException,
			IOException {

		HttpGet httpGet = new HttpGet(url);

		return processResponse(client.execute(httpGet));

	}

	private String makePostRequest(String url,
			List<NameValuePair> nameValuePairs) throws ClientProtocolException,
			IOException {

		HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		return processResponse(client.execute(httpPost));

	}

	private ArrayList<SavvyItem> generateSavvyItemArrayList(
			String itemResponseString) throws JSONException {
		ArrayList<SavvyItem> items = new ArrayList<SavvyItem>();
		JSONObject itemsJSON = new JSONObject(itemResponseString);

		JSONArray itemsJSONArray = (JSONArray) itemsJSON.get("items");

		for (int i = 0; i < itemsJSONArray.length(); i++) {
			SavvyItem item = new SavvyItem();
			item.setCatId(((JSONObject) itemsJSONArray.get(i)).getInt("catid"));
			item.setId(((JSONObject) itemsJSONArray.get(i)).getInt("id"));
			item.setLat(new Float(((JSONObject) itemsJSONArray.get(i))
					.getString("lat")));
			item.setLon(new Float(((JSONObject) itemsJSONArray.get(i))
					.getString("lon")));
			item.setName(((JSONObject) itemsJSONArray.get(i)).getString("name"));
			item.setPrice(((JSONObject) itemsJSONArray.get(i))
					.getDouble("price"));
			item.setQuantity(((JSONObject) itemsJSONArray.get(i))
					.getInt("quantity"));
			item.setUserId(((JSONObject) itemsJSONArray.get(i))
					.getInt("userid"));
			item.setTs(((JSONObject) itemsJSONArray.get(i)).getString("ts"));
			item.setRating(new Float(((JSONObject) itemsJSONArray.get(i))
					.getString("rating")));
			items.add(item);
		}

		return items;
	}

	public ArrayList<SavvyCategory> getCategories() throws JSONException,
			ClientProtocolException, IOException {
		String categoryResponseString = this.makeGetRequest(BASE_URL
				+ "/category");
		ArrayList<SavvyCategory> categories = new ArrayList<SavvyCategory>();
		JSONObject categoryJSON = new JSONObject(categoryResponseString);

		// parse JSON
		JSONArray categoryJSONArray = (JSONArray) categoryJSON
				.get("categories");

		for (int i = 0; i < categoryJSONArray.length(); i++) {
			SavvyCategory category = new SavvyCategory();
			category.setName(((JSONObject) categoryJSONArray.get(i))
					.getString("name"));
			category.setId(((JSONObject) categoryJSONArray.get(i)).getInt("id"));
			category.setPicUrl(((JSONObject) categoryJSONArray.get(i))
					.getString("picurl"));
			categories.add(category);
		}
		return categories;

	}

	public ArrayList<SavvyItem> getItemsForCategoryByName(String categoryName)
			throws JSONException, ClientProtocolException, IOException {
		String itemResponseString = this.makeGetRequest(BASE_URL + "/items/"
				+ categoryName + "/");
		return this.generateSavvyItemArrayList(itemResponseString);

	}

	public ArrayList<SavvyItem> getItemsForCategoryByCategoryId(int categoryID)
			throws JSONException, ClientProtocolException, IOException {

		String itemResponseString = this.makeGetRequest(BASE_URL + "/items/"
				+ Integer.toString(categoryID) + "/");
		return this.generateSavvyItemArrayList(itemResponseString);

	}

	public ArrayList<SavvyItem> findItemsInRadiusOfLatLon(float lat, float lon,
			double radius) throws JSONException, ClientProtocolException,
			IOException {

		String itemResponseString = this.makeGetRequest(BASE_URL
				+ "/search/within/" + Float.toString(lat) + ","
				+ Float.toString(lon) + "," + Double.toString(radius) + "/");

		return this.generateSavvyItemArrayList(itemResponseString);

	}

	public ArrayList<SavvyItem> findItemsInRadiusOfLatLon(String name,
			float lat, float lon, double radius) throws JSONException,
			ClientProtocolException, IOException {

		String itemResponseString = this.makeGetRequest(BASE_URL + "/search/"
				+ name + "/within/" + Float.toString(lat) + ","
				+ Float.toString(lon) + "," + Double.toString(radius) + "/");

		return this.generateSavvyItemArrayList(itemResponseString);

	}

	public ArrayList<SavvyItem> findItemsInRadiusOfLatLon(String name,
			float lat, float lon, double radius, double ageInHours)
			throws JSONException, ClientProtocolException, IOException {

		String itemResponseString = this.makeGetRequest(BASE_URL + "/search/"
				+ name + "/within/" + Float.toString(lat) + ","
				+ Float.toString(lon) + "," + Double.toString(radius) + "/"
				+ Double.toString(ageInHours) + "/");
		return this.generateSavvyItemArrayList(itemResponseString);

	}

	public void registerUser(String name, String email, String regId)
			throws ClientProtocolException, IOException {

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("name", name));
		nameValuePairs.add(new BasicNameValuePair("emai", email));
		nameValuePairs.add(new BasicNameValuePair("regId", regId));

		//secure_base_url might not be working via server configuration.  I will fix it.
		this.makePostRequest(BASE_URL+"/gcm/register.php", nameValuePairs);

	}

}
