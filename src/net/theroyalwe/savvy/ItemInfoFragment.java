package net.theroyalwe.savvy;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

public class ItemInfoFragment extends Fragment{
	
	SavvyItem mitem;
	
	@Override
	public void onCreate (Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		ItemParceable itemParceable = (ItemParceable) getArguments().getParcelable(getString(R.string.ITEM_KEY));
		mitem = itemParceable.getItem();
	}
	
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState); 
	    TextView title = (TextView) getView().findViewById(R.id.itemInfoTitle);
	    title.setText(mitem.getName());
	    
	    TextView price = (TextView) getView().findViewById(R.id.itemInfoPrice);
	    price.setText(mitem.getName());
	    
	    RatingBar ratingBar = (RatingBar) getView().findViewById(R.id.itemInfoRatingBar);
	    ratingBar.setRating(mitem.getRating());
	    
	    final Button goToMapButton = (Button) getView().findViewById(R.id.itemInfoMapViewButton);
        goToMapButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
            }
        });
	  }

	 	  
	  
}

