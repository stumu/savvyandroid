package net.theroyalwe.savvy;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

public class CategoryListFragment extends ListFragment{
	
	private static String TAG = "CATEGORY";
	
	
	private static ArrayList<SavvyCategory> categories = new ArrayList<SavvyCategory>();
	
	
	
	@Override
	  public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState);
	    new GetCategoriesTask().execute();
	     
	  }

	  @Override
	  public void onListItemClick(ListView l, View v, int position, long id) {
	    // Do something with the data
		Log.v("Category clicked", categories.get(position).name);
		  
		   
	  }
	  
	  
	  private void updateUI(){
		  SavvyCategory [] categoryArray = categories.toArray(new SavvyCategory[categories.size()]);
		  CategoryAdapter adapter = new CategoryAdapter(getActivity(), R.layout.listview_item_row, categoryArray);
		  setListAdapter(adapter);
	  }
	  
	  private class GetCategoriesTask extends AsyncTask<String, Integer, Boolean>{

		  @Override
		  protected void onPreExecute(){
			  //show a loader
		  }
		  
		  @Override
		  protected void onPostExecute(Boolean result){
			  //update The list, update UI
			  updateUI();
		  }
		  
		  
			@Override
			protected Boolean doInBackground(String... args) {
				Log.v(TAG, "in Background doing stuff");
				//make the http call
				
				try {
					
					categories = new SavvyAPI().getCategories();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return null;
			}
			
		}

	  
	  
	  
}

